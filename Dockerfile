FROM dikeert/jdk:16.04

# basic stuff
RUN apt-get -y install \
	sudo \
	ssh \
	curl \
	wget \
	ruby \
	git

# not so basic stuff
RUN apt-get -y install \
	mysql-client \
	postgresql-client \
	tmux

#Gradle
RUN apt-get -y install gradle

#Instead of using maven I use Maven Version Manager (http://mvnvm.org/)
RUN curl -s https://bitbucket.org/mjensen/mvnvm/raw/master/mvn > \
		/usr/local/bin/mvn && \
	chmod 0755 /usr/local/bin/mvn

#Docker-compose
RUN curl -L \
	https://github.com/docker/compose/releases/download/1.8.0/docker-compose-`uname -s`-`uname -m` > \
		/usr/local/bin/docker-compose && \
	chmod +x /usr/local/bin/docker-compose

#Create a dev user
RUN useradd -m -G users,staff dev && \
	chsh -s /bin/bash dev && \
	echo "dev:dev" | chpasswd && \
	bash -c 'echo "dev ALL=(ALL) NOPASSWD:ALL" | (EDITOR="tee -a" visudo)' 

USER dev
WORKDIR /home/dev

RUN mkdir /home/dev/.ssh && \
	ssh-keygen -b 2048 -t rsa -f /home/dev/.ssh/docker_key -q -N "" && \
	cat /home/dev/.ssh/docker_key.pub >> /home/dev/.ssh/authorized_keys
COPY config /home/dev/.ssh/

USER root
WORKDIR / 

ADD entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

ENTRYPOINT /entrypoint.sh
