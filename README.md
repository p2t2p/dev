# README #

This is a docker image with basic tools required to do java development.

Use docker-compose to build and run it.

### Build it! ###
```bash
docker-compose build
```

_It will generate new unique ssh key for you. You can safely use it for real-world authorisations_

### Run it! ###
```bash
docker-compose up -d
```

_if you did download this image from docker hub instead of building_
_it by yourself DO NOT USE ssh key provided with this image
_for any real-world authorizations_

### Use it! ###

```bash
docker cp devbox:/home/dev/.ssh/docker_key ~/.ssh/docker_key
docker cp devbox:/home/dev/.ssh/config ~/.ssh/docker_config
cat ~/.ssh/docker_config >> ~/.ssh/config
ssh devbox
```